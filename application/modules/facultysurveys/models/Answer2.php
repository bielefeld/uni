<?php


class Answer2
{
    /**
     * @var int the survey id
     */
    private $_surveyId;
    private $_clang;

    /**
     * @param $fieldcode
     * @param $value
     * @param string $format
     * @param string $dateformatphp
     * @return bool|string
     */
    function get_extendedanswer($fieldcode, $value, $format = '', $dateformatphp = 'd.m.Y')
    {
        global $connect, $action;

        //get SID
        $ids = explode("X", $fieldcode);
        if (is_numeric($ids[0])) {
            $this->_surveyId = $ids[0];
        }
        // use Survey base language if s_lang isn't set in _SESSION (when browsing answers)
        //get the survey's base language
        $baselanguage = GetBaseLanguageFromSurveyID($this->_surveyId);

        //initialize $clang for multilingual surveys
        $this->_clang = SetSurveyLanguage($this->_surveyId, $baselanguage);

        $s_lang = GetBaseLanguageFromSurveyID($this->_surveyId);
        if (!isset($action) || (isset($action) && $action != 'browse')) {
            if (isset($_SESSION['s_lang'])) $s_lang = $_SESSION['s_lang'];  //This one does not work in admin mode when you browse a particular answer
        }

        //Fieldcode used to determine question, $value used to match against answer code
        //Returns NULL if question type does not suit
        if (substr_count($fieldcode, "X") > 1) //Only check if it looks like a real fieldcode
        {
            $fieldmap = createFieldMap($this->_surveyId);

            if (isset($fieldmap[$fieldcode])) {
                $fields = $fieldmap[$fieldcode];
            } else {
                return false;
            }

            //Find out the question type
            $this_type = $fields['type'];

            list($value, $this_answer) = $this->processFieldMap($fieldcode, $value, $dateformatphp, $this_type, $fields, $connect, $s_lang);
        }
        if (isset($this_answer)) {
            if ($format != 'INSERTANS') {
                return $this_answer . " [$value]";
            } else {
                if (strip_tags($this_answer) == "") {
                    switch ($this_type) {// for questions with answers beeing
                        // answer code, it is safe to return the
                        // code instead of the blank stripped answer
                        case "A":
                        case "B":
                        case "C":
                        case "E":
                        case "F":
                        case "H":
                        case "1":
                        case "M":
                        case "P":
                        case "!":
                        case "5":
                        case "L":
                        case "O":
                            return $value;
                            break;
                        default:
                            return strip_tags($this_answer);
                            break;
                    }
                } else {
                    return strip_tags($this_answer);
                }
            }
        } else {
            return $value;
        }
    }

    /**
     * @param $fieldcode
     * @param $value
     * @param $dateformatphp
     * @param $this_type
     * @param $fields
     * @param $connect
     * @param $s_lang
     * @return array
     */
    private function processFieldMap($fieldcode, $value, $dateformatphp, $this_type, $fields, $connect, $s_lang): array
    {
        switch ($this_type) {
            case 'D':
                if (trim($value) != '') {
                    $datetimeobj = new Date_Time_Converter($value, "Y-m-d H:i:s");
                    $value = $datetimeobj->convert($dateformatphp);
                }
                break;
            case "L":
            case "!":
            case "O":
            case "^":
            case "I":
            case "R":
                $query = "SELECT code, answer FROM " . db_table_name('answers') . " WHERE qid={$fields['qid']} AND code='" . $connect->escape($value) . "' AND scale_id=0 AND language='" . $s_lang . "'";
                $result = db_execute_assoc($query) or safe_die("Couldn't get answer type L - getextendedanswer() in common.php<br />$query<br />" . $connect->ErrorMsg()); //Checked
                while ($row = $result->FetchRow()) {
                    $this_answer = $row['answer'];
                } // while
                if ($value == "-oth-") {
                    $this_answer = $this->_clang->gT("Other");
                }
                break;
            case "M":
            case "J":
            case "P":
                switch ($value) {
                    case "Y":
                        $this_answer = $this->_clang->gT("Yes");
                        break;
                }
                break;
            case "Y":
                switch ($value) {
                    case "Y":
                        $this_answer = $this->_clang->gT("Yes");
                        break;
                    case "N":
                        $this_answer = $this->_clang->gT("No");
                        break;
                    default:
                        $this_answer = $this->_clang->gT("No answer");
                }
                break;
            case "G":
                switch ($value) {
                    case "M":
                        $this_answer = $this->_clang->gT("Male");
                        break;
                    case "F":
                        $this_answer = $this->_clang->gT("Female");
                        break;
                    default:
                        $this_answer = $this->_clang->gT("No answer");
                }
                break;
            case "C":
                switch ($value) {
                    case "Y":
                        $this_answer = $this->_clang->gT("Yes");
                        break;
                    case "N":
                        $this_answer = $this->_clang->gT("No");
                        break;
                    case "U":
                        $this_answer = $this->_clang->gT("Uncertain");
                        break;
                }
                break;
            case "E":
                switch ($value) {
                    case "I":
                        $this_answer = $this->_clang->gT("Increase");
                        break;
                    case "D":
                        $this_answer = $this->_clang->gT("Decrease");
                        break;
                    case "S":
                        $this_answer = $this->_clang->gT("Same");
                        break;
                }
                break;
            case "F":
            case "H":
            case "1":
                $query = "SELECT answer FROM " . db_table_name('answers') . " WHERE qid={$fields['qid']} AND code='" . $connect->escape($value) . "' AND language='" . $s_lang . "'";
                if (isset($fields['scale_id'])) {
                    $query .= " AND scale_id={$fields['scale_id']}";
                }
                $result = db_execute_assoc($query) or safe_die("Couldn't get answer type F/H - getextendedanswer() in common.php");   //Checked
                while ($row = $result->FetchRow()) {
                    $this_answer = $row['answer'];
                } // while
                if ($value == "-oth-") {
                    $this_answer = $this->_clang->gT("Other");
                }
                break;
            case "|": //File upload
                if (substr($fieldcode, -9) == 'filecount') {
                    $this_answer = $this->_clang->gT("File count");
                } else {
                    //Show the filename, size, title and comment -- no link!
                    $files = json_decode($value);
                    $value = '';
                    if (is_array($files)) {
                        foreach ($files as $file) {
                            $value .= $file->name .
                                ' (' . $file->size . 'KB) ' .
                                strip_tags($file->title) .
                                ' - ' . strip_tags($file->comment) . "<br/>";
                        }
                    }
                }
                break;
            default:
                ;
        }
        return array($value, $this_answer); // switch
    }
}
