<?php


namespace application\modules\facultysurveys;

use Yii;

class FacultySurveysModule extends \CWebModule
{
    /** @var string */
    private $responsible;
    /** @var string */
    private $department;
    /** @var string */
    private $furtherData;
    /** @var string */
    private $faculty;
    /** @var string */
    private $facultyMode;

    /* @var string */
    private $assetsUrl;

    /* @var string */
    public $defaultController = 'module';

    /** @var int the registration survey id */

    public function __construct($id, $parent, $config = null)
    {
        parent::__construct($id, $parent, $config);
    }


    /**
     * Returns the assets URL.
     * Assets folder has few orphan and very useful utility libraries.
     * @return string
     */
    public function getAssetsUrl()
    {
        if ($this->assetsUrl === null) {
            $this->assetsUrl = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('facultysurveys.assets'));
        }
        return $this->assetsUrl;
    }

    public function SendRegistrationMail(int $surveyId)
    {
        $bounceMail = getBounceEmail($surveyId);
    }

    /**
     * @return string
     */
    public function getDepartment(): string
    {
        return $this->department;
    }

    /**
     * @return string
     */
    public function getFaculty(): string
    {
        return $this->faculty;
    }

    /**
     * @return string
     */
    public function getFacultyMode(): string
    {
        return $this->facultyMode;
    }

    /**
     * @return string
     */
    public function getFurtherData(): string
    {
        return $this->furtherData;
    }

    /**
     * @return string
     */
    public function getResponsible(): string
    {
        return $this->responsible;
    }

    /**
     * @param string $responsible
     */
    public function setResponsible($responsible): void
    {
        $this->responsible = $responsible;
    }

    /**
     * @param string $department
     */
    public function setDepartment($department): void
    {
        $this->department = $department;
    }

    /**
     * @param string $furtherData
     */
    public function setFurtherData($furtherData): void
    {
        $this->furtherData = $furtherData;
    }

    /**
     * @param string $faculty
     */
    public function setFaculty($faculty): void
    {
        $this->faculty = $faculty;
    }

    /**
     * @param string $facultyMode
     */
    public function setFacultyMode($facultyMode): void
    {
        $this->facultyMode = $facultyMode;
    }

    /** @var string[] the texts */
    private $texts;

    /** translates the given text into the given language
     * @param string $language
     * @param string $text
     * @return string
     */
    public function t(string $language, string $text): string
    {
        $language = strtolower($language);
        if (!isset($this->texts)) {
            $this->texts = require __DIR__ . "/language/{$this->facultyMode}-{$language}.php";
        }
        return $this->texts[$text];
    }

    public function calculateId(int $savedId, int $surveyId): int
    {
        $unsaltedId = substr((string)$savedId, 5);
        $unsaltedId = substr((string)$unsaltedId, 0, strlen($unsaltedId) - 3);

        $calulatedId = ((intval($unsaltedId) / 3) + 7) / $surveyId;
        if(is_float($calulatedId))
            return 0;
        return $calulatedId;
    }

}
