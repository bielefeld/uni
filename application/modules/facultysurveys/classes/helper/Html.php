<?php

namespace application\modules\facultysurveys\classes\helper;

use application\modules\facultysurveys\classes\QuestionData;
use application\modules\facultysurveys\classes\ResponseData;

class Html
{
    /** @var string[] */
    private $chapterCssClasses = [];
    /**
     * @var int
     */
    private $currentChapter = 0;
    /** @var array[] */
    private $elements = [];
    /**
     * @var int
     */
    private $fileCount;
    /**
     * @var bool
     */
    private $printMode;
    private $surveyId;
    /**
     * @var string
     */
    private $urlDownload;
    /**
     * @var string
     */
    private $urlPrint;

    public function __construct(bool $printMode, string $urlPrint, string $urlDownload)
    {
        $this->printMode = $printMode;
        $this->urlPrint = $urlPrint;
        $this->urlDownload = $urlDownload;
    }

    public function createHeader(string $newHeader)
    {
        $this->appendFull('h4', $newHeader);
    }

    public function createMainButtons(): void
    {
        if ($this->printMode) {
            return;
        }
        $this->appendFull('script', 'function hide() {'
            . '$(\'button.display\').click();'
            . '$(\'h4.text-info\').each(function(index){const chapter = $(this).data(\'chapter\'); $(\'div.\'+chapter).hide();$(\'*[data-chapter="\'+chapter+\'"]\').hide() });};');
        $this->appendFull('div', '###MAINBUTTONS###', ['class' => 'container', 'style' => 'padding-bottom: 10px']);
    }

    public function createMultipleRows(array $content): void
    {
        $key = self:: array_key_first($content);
        $this->appendOpenTag('div', ['class' => 'row']);
        $this->appendOpenTag('div', ['class' => 'col-sm-12']);
        $this->appendFull('strong', $key, ['class' => 'text-info']);
        $this->appendCloseTag('div');
        $this->appendCloseTag('div');
        foreach ($content[$key] as $item) {
            $this->createListEntry($item['content'], $item['response']);
        }
    }

    private static function array_key_first(array $arr)
    {
        foreach ($arr as $key => $unused) {
            return $key;
        }
        return null;
    }

    /**
     * @param string $tag
     * @param array|null $properties
     */
    public function appendOpenTag(string $tag, array $properties = null)
    {
        $cString = "c$this->currentChapter";
        $propertiesHtml = '';
        $propertyCollection = [];
        if (isset($properties)) {
            foreach ($properties as $index => $property) {
                $propertyCollection[] = "{$index}='$property'";
            }
            $propertiesHtml = ' ' . implode(' ', $propertyCollection);
        }
        $this->elements[$cString][] = "<$tag$propertiesHtml>";
    }

    /**
     * @param string $tag
     * @param string $content
     * @param array|null $properties
     */
    public function appendFull(string $tag, ?string $content, array $properties = null): void
    {
        $cString = "c$this->currentChapter";
        if (!isset($this->elements[$cString])) {
            $this->elements[$cString] = [];
        }

        $propertiesHtml = '';
        $propertyCollection = [];
        if (isset($properties)) {
            foreach ($properties as $index => $property) {
                $propertyCollection[] = "{$index}=\"$property\"";
            }
            $propertiesHtml = ' ' . implode(' ', $propertyCollection);
        }
        $this->elements[$cString][] = "<$tag$propertiesHtml>$content</$tag>";
    }

    /**
     * @param int $chapter
     * @param string $tag
     */
    public function appendCloseTag(string $tag)
    {
        $cString = "c$this->currentChapter";
        if (!isset($this->elements[$cString])) {
            $this->elements[$cString] = [];
        }
        $this->elements[$cString][] = "</$tag>";
    }

    /** Creates a new row in the view
     * @param string $questionText the question text
     * @param string|null $responseText the response text
     */
    public function createListEntry(string $questionText, string $responseText): void
    {
        if ($responseText === '-oth-') {
            $responseText = gT('Other');
        }
        $class = 'text-print ';
        if ($responseText === gT('Yes')) {
            $class .= 'text-success';
        } elseif ($responseText === gT('No')) {
            $class .= 'text-danger';
            $this->chapterCssClasses[$this->currentChapter] = 'text-danger';
        }
        $this->appendOpenTag('div', ['class' => 'row']);
        $this->appendOpenTag('div', ['class' => 'col-sm-6', 'style' => 'margin-left: 25px']);
        $this->appendFull('p', $questionText, ['class' => 'text-info q-header']);
        $this->appendCloseTag('div');
        $this->appendOpenTag('div', ['class' => 'col-sm-6', 'style' => 'margin-left: -25px']);
        $this->appendFull('p', $responseText, ['class' => "$class"]);
        $this->appendCloseTag('div');
        $this->appendCloseTag('div');
    }

    /** Creates a new row in the view
     * @param string $questionText the question text
     * @param string|null $responseText the response text
     * @param QuestionData|null $questionData
     */
    public function createRow(string $questionText, ?string $responseText, QuestionData $questionData = null): void
    {
        if ($responseText === '-oth-') {
            $responseText = gT('Other');
        }
        $class = 'text-print ';

        if ($responseText === gT('Yes') || strpos($questionData->parent->title, 'Hilfe') !== false) {
            $class .= 'text-success';
        } elseif ($responseText === gT('No')) {
            $class .= 'text-danger';
            $this->chapterCssClasses[$this->currentChapter] = 'text-danger';
        }

        $this->appendOpenTag('div', ['class' => 'row']);
        $this->appendOpenTag('div', ['class' => 'col-sm-6']);
        $this->appendFull('p', $questionText, ['class' => 'text-info q-header']);
        $this->appendCloseTag('div');
        $this->appendOpenTag('div', ['class' => 'col-sm-6']);
        $this->appendFull('p', $responseText, ['class' => "$class"]);
        $this->appendCloseTag('div');
        $this->appendCloseTag('div');
    }

    public function endChapter(): void
    {
        $this->appendCloseTag('div');
    }

    /**
     * @param int $chapter
     * @param QuestionData $question
     * @param ResponseData $response
     */
    public function openNewChapter(string $text): void
    {
        $this->currentChapter++;
        $this->createChapterButtons($text);
        $this->appendOpenTag('div', [
            'class' => "chapter_{$this->currentChapter} container"
        ]);
    }

    /** Creates the show and hide buttons for the given chapter
     * @param string $parent
     */
    private function createChapterButtons(string $parent): void
    {
        $this->chapterCssClasses[$this->currentChapter] = 'text-info';
        $this->appendOpenTag('div', ['class' => "container"]);
        $this->appendOpenTag('div', ['class' => 'row']);
        $this->appendOpenTag('div', ['class' => 'col-sm-2 btn-show-hide']);
        if ($this->printMode === false) {
            $this->appendFull('button', '+',
                [
                    'class' => 'btn btn-light btn-sm display',
                    'style' => 'margin-right: 5px',
                    'onclick' => "$('.chapter_{$this->currentChapter}').show();",
                    'data-chapter' => "chapter_{$this->currentChapter}"
                ]);
            $this->appendFull('button', '-',
                [
                    'class' => 'btn btn-light btn-sm dont-display',
                    'onclick' => "$('.chapter_{$this->currentChapter}').hide();",
                    'data-chapter' => "chapter_{$this->currentChapter}"
                ]);
        }
        $this->appendCloseTag('div');
        $this->appendOpenTag('div', ['class' => 'col-sm-7']);

        $this->appendFull('h4', "{$parent}",
            [
                'class' => "###{$this->currentChapter}### bf-header",
                'data-chapter' => "chapter_{$this->currentChapter}"
            ]);
        $this->appendCloseTag('div');
        $this->appendCloseTag('div');
        $this->appendCloseTag('div');
    }

    /** sets the file count */
    public function setFileCount(int $count): void
    {
        $this->fileCount = $count;
    }

    public function toString(): string
    {
        $html = '';
        foreach ($this->elements as $element) {
            /* @var string[] $element */
            foreach ($element as $item) {
                $html .= $item;
            }
        }
        foreach ($this->chapterCssClasses as $chapter => $class) {
            $html = str_replace("###$chapter###", $class, $html);
        }
        $this->createButtons($html);

        return $html;
    }

    private function createButtons(string &$html): void
    {
        $config = [
            [
                'href' => '#',
                'text' => 'GBU zur Dokumentation',
                'onclick' => '$(\'button.dont-display\').click();'
                    . '$(\'*[data-chapter]\').show();'
                    . '$(\'h4\').parent().parent().find(\'button.display\').click();'
            ],
            [
                'href' => '#',
                'text' => 'Zeige nicht erfüllte Auflagen',
                'onclick' => 'hide()'
            ],
            [
                'href' => '#',
                'text' => 'Zeige nur &Uuml;bersicht',
                'onclick' => '$(\'*[data-chapter]\').show(); $(\'button.dont-display\').click();'
            ],
            [
                'href' => '#',
                'text' => 'Seite drucken',
                'onclick' => "window.location.href='{$this->urlPrint}';"
            ]
        ];
        if ($this->fileCount > 0) {
            $file = $this->fileCount == 1 ? 'Datei' : 'Dateien';
            $config[] = [
                'href' => '#',
                'text' => "{$this->fileCount} $file herunterladen",
                'onclick' => "window.open('{$this->urlDownload}', '_blank');"
            ];
        }
        $html = str_replace('###MAINBUTTONS###', $this->createButtonsFromConfig($config), $html);
    }

    private function createButtonsFromConfig(array $config): string
    {
        $buttons = '';
        foreach ($config as $button) {
            $html = '';
            foreach ($button as $tag => $value) {
                if ($tag === 'text') {
                    continue;
                }
                $html .= "$tag=\"$value\" ";
            }
            $buttons .= "<button class='btn btn-light btn-sm' style='margin-right: 5px' $html>{$button['text']}</button>";
        }
        return $buttons;
    }
}
