<?php


namespace application\modules\facultysurveys\classes;

use Yii;

class FileDownloader
{
    /**
     * @var int
     */
    private $responseId;
    /** @var int the survey id */
    private $surveyId;
    /** @var string the file name */
    private $zipfilename;

    public function __construct(int $surveyId, int $responseId)
    {
        $this->surveyId = $surveyId;
        $this->responseId = $responseId;
        $this->zipfilename = "Files_for_survey_{$this->surveyId}.zip";

        Yii::app()->loadHelper('common');
        Yii::app()->loadHelper('sanitize');
        Yii::app()->loadLibrary('admin/pclzip');
    }

    /**
     * @param $responseIds
     * @return bool
     */
    public function createDownload(): bool
    {
        $separator = DIRECTORY_SEPARATOR;
        $tmpdir = Yii::app()->getConfig('uploaddir') . $separator . "surveys" . $separator
            . $this->surveyId . $separator . "files" . $separator;

        $fileList = array();
        $response = \Response::model($this->surveyId)->findByPk($this->responseId);
        $fileCount = 0;
        foreach ($response->getFiles() as $file) {
            $fileCount++;
            /*
            * Now add the file to the archive, prefix files with responseid_index to keep them
            * unique. This way we can have 234_1_image1.gif, 234_2_image1.gif as it could be
            * files from a different source with the same name.
            */
            if (file_exists($tmpdir . basename($file['filename']))) {
                $fileList[] = array(
                    PCLZIP_ATT_FILE_NAME => $tmpdir . basename($file['filename']),
                    PCLZIP_ATT_FILE_NEW_FULL_NAME => sprintf("%05s_%02s_%s", $response->id, $fileCount,
                        sanitize_filename(rawurldecode($file['name'])))
                );
            }
        }

        if (count($fileList) > 0) {
            $zip = new \PclZip($tmpdir . $this->zipfilename);
            if ($zip->create($fileList) === 0) {
                return false;
            }

            if (file_exists($tmpdir . '/' . $this->zipfilename)) {
                $this->createHeader($tmpdir);
                return true;
            }
        }
        return false;
        // No files : redirect to browse with a alert
//        Yii::app()->setFlashMessage(gT("Sorry, there are no files for this response."), 'error');
//        $this->getController()->redirect(array("admin/responses", "sa" => "browse", "surveyid" => $this->surveyId));
    }

    /**
     * @param string $tmpdir
     */
    private function createHeader(string $tmpdir): void
    {
        @ob_clean();
        header('Content-Description: File Transfer');
        header('Content-Type: application/zip, application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($this->zipfilename));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header("Cache-Control: must-revalidate, no-store, no-cache");
        header('Content-Length: ' . filesize($tmpdir . "/" . $this->zipfilename));
        readfile($tmpdir . '/' . $this->zipfilename);
        unlink($tmpdir . '/' . $this->zipfilename);
    }
}
