<?php

namespace application\modules\facultysurveys\classes;

use CException;
use CHtml;
use Token;
use Yii;

class Mailer
{
    /**
     * @var RegistrationData
     */
    private $data;

    /**
     * Mailer constructor.
     * @param RegistrationData $data
     */
    private function __construct(RegistrationData $data)
    {
        Yii::app()->loadHelper('common');
        Yii::app()->loadHelper('replacements');
        Yii::app()->loadHelper('expressions.em_manager');
        $this->data = $data;
    }

    /** Processes the registration data
     * @param RegistrationData $data
     * @throws CException
     */
    public static function sendNotification(RegistrationData $data)
    {
        $mailer = new Mailer($data);
        $mailer->execute();
    }

    /** Executes the survey notification via mail
     * @throws CException
     */
    private function execute(): void
    {
        $initialSurvey = $this->data->surveys[0];
        $followingSurveys = array();
        for ($i = 1; $i < count($this->data->surveys); $i++) {
            $followingSurveys[] = $this->data->surveys[$i];
        }
        $this->SendMail($initialSurvey, $followingSurveys);
    }

    /** sends the invitation mail for a specific survey
     * @param \Survey $initialSurvey the survey
     * @param \Survey[] $followSurveys the following surveys
     * @throws CException
     */
    private function SendMail(\Survey $initialSurvey, array $followSurveys): void
    {
        $surveyInfo = getSurveyInfo($initialSurvey->sid, 'de');

        $body = 'surveyls_email_invite';
        $body = isset($surveyInfo[$body])
            ? $surveyInfo[$body]
            : '';

        $subject = 'surveyls_email_invite_subj';
        $subject = isset($surveyInfo[$subject]) ? $surveyInfo[$subject] : '';

        $token = Token::model($initialSurvey->sid)->findByToken($this->data->token);

        if (empty($subject)) {
            throw new CException('Subject should not be empty');
        }
        if (empty($body)) {
            throw new CException('Body should not be empty');
        }

        $fieldsArray = $this->createSurveyConfig($initialSurvey, $token, $surveyInfo);

        $subject = Replacefields($subject, $fieldsArray);
        $body = Replacefields($body, $fieldsArray);

        if (count($followSurveys) > 0) {
            $body .= '<h3>Folgeumfrage' . (count($followSurveys) > 1 ? 'n' : '') . '</h3>';
            foreach ($followSurveys as $survey) {
                $body .= $this->createUrlsForSurvey($survey, $token);
            }
        }

        SendEmailMessage(
            $body,
            $subject,
            $token->email,
            "{$surveyInfo['adminname']} <{$surveyInfo['adminemail']}>",
            Yii::app()->getConfig('sitename'),
            true,
            getBounceEmail($initialSurvey->sid),
            null,
            $this->getCustomHeaders($token)
        );
    }

    /**
     * @param \Survey $initialSurvey
     * @param Token $token
     * @param $surveyInfo
     * @return array
     */
    private function createSurveyConfig(\Survey $initialSurvey, Token $token, $surveyInfo): array
    {
        $surveyUrl = $this->createSurveyUrl($initialSurvey, $token, '/survey/index');
        $optOutUrl = $this->createActionUrl($initialSurvey, $token, '/optout/tokens');
        $optInUrl = $this->createActionUrl($initialSurvey, $token, '/optin/tokens');
        // replace token attributes like {ATTRIBUTE_3}
        $fieldsArray = [];
        foreach ($token->attributes as $attribute => $value) {
            $fieldsArray['{' . strtoupper($attribute) . '}'] = $value;
        }
        $fieldsArray['{SURVEYNAME}'] = $surveyInfo['surveyls_title'];
        $fieldsArray['{ADMINNAME}'] = $surveyInfo['adminname'];
        $fieldsArray['{ADMINEMAIL}'] = $surveyInfo['adminemail'];
        $fieldsArray['{SURVEYDESCRIPTION}'] = $surveyInfo['surveyls_description'];
        $fieldsArray['{SURVEYURL}'] = CHtml::link('zur Umfrage', $surveyUrl);
        $fieldsArray['{OPTOUTURL}'] = CHtml::link('Aus Umfrage abmelden', $optOutUrl);
        $fieldsArray['{OPTINURL}'] = CHtml::link('F&uuml;r die Umfrage freischalten', $optInUrl);
        return $fieldsArray;
    }

    /** Creates a survey url
     * @param \Survey $survey
     * @param Token $token
     * @param string $url
     * @return string
     */
    private function createSurveyUrl(\Survey $survey, Token $token, $url): string
    {
        $surveyUrl = Yii::app()->createAbsoluteUrl($url, array(
            'sid' => $survey->sid,
            'token' => $token->token,
//            'lang' => $token->language,
            'newtest' => 'Y',
        ));
        return $surveyUrl;
    }

    /** Creates a survey url
     * @param \Survey $survey
     * @param Token $token
     * @param string $url
     * @return string
     */
    private function createActionUrl(\Survey $survey, Token $token, $url): string
    {
        $surveyUrl = Yii::app()->createAbsoluteUrl($url, array(
            'surveyid' => $survey->sid,
            'token' => $token->token,
//            'lang' => $token->language,
        ));
        return $surveyUrl;
    }

    /** Creates the urls for the given survey
     * @param \Survey $survey
     * @param string $body
     * @param Token $token
     * @return string
     */
    private function createUrlsForSurvey(\Survey $survey, Token $token): string
    {
        $html = '';
        $surveyInfoFollow = getSurveyInfo($survey->sid, 'de');
        $html .= '<b>' . $surveyInfoFollow['surveyls_title'] . '</b>';
        $html .= '<br>' . CHtml::link('zur Umfrage', $this->createSurveyUrl($survey, $token, '/survey/index'));
//        $html .= '<br>' . CHtml::link('F&uuml;r die Umfrage freischalten',
//                $this->createActionUrl($survey, $token, '/optin/tokens'));
//        $html .= '<br>' . CHtml::link('Aus Umfrage abmelden',
//                $this->createActionUrl($survey, $token, '/optout/tokens'));
        $html .= '<br><br>';
        return $html;
    }

    /**
     * @param Token $token
     * @return array
     */
    private function getCustomHeaders(Token $token): array
    {
        $customHeaders = array(
            '1' => 'X-surveyid: ' . $this->data->surveys[0]->sid,
            '2' => 'X-tokenid: ' . $token->token
        );
        return $customHeaders;
    }
}
