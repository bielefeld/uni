<?php


namespace application\modules\facultysurveys\classes;


class RegistrationToken extends \Token
{
    public function __construct($scenario = 'insert', int $sId)
    {
        $this->dynamicId = $sId;
        parent::__construct($scenario);
    }
}
