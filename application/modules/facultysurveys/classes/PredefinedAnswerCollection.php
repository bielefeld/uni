<?php

namespace application\modules\facultysurveys\classes;

class PredefinedAnswerCollection
{
    /** @var \CDbConnection */
    private $db;
    /** @var array $storage Contains the predefined answers per qid of the given survey */
    private $storage;
    /**
     * @var int
     */
    private $surveyId;
    /**
     * @var string
     */
    private $language;

    /**
     * PredefinedAnswerCollection constructor.
     *
     * @param int $surveyId
     *
     * @throws \CException
     */
    private function __construct(int $surveyId, string $language)
    {
        $this->db = \Yii::app()->db;
        $this->surveyId = $surveyId;
        $this->language = $language;
        $this->loadAnswers();
    }

    /**
     * @throws \CException
     */
    private function loadAnswers(): void
    {
        $command = $this->db->createCommand($this->getSqlQuery());
        $command->bindParam(':sid', $this->surveyId);
        $command->bindParam(':language', $this->language);

        $data = $command->queryAll();

        foreach ($data as $item) {
            if (!isset($this->storage[$item['qid']])) {
                $this->storage[$item['qid']] = [];
            }
            $this->storage[$item['qid']][$item["code"]] = $item['answer'];
        }
    }

    private function getSqlQuery(): string
    {
        return 'SELECT a.qid, a.code, a.answer '
            . "  FROM {$this->db->tablePrefix}answers a "
            . "  INNER JOIN {$this->db->tablePrefix}questions q ON q.qid=a.qid AND q.language=a.language "
            . '  WHERE q.sid=:sid '
            . ' AND q.language=:language '
            . ' AND a.language=:language;';
    }

    /**
     * @param string $qid
     * @param string $code
     *
     * @return string
     * @throws \Exception
     */
    public function getAnswer(string $qid, ?string $code): ?string
    {
        $targetQid = $qid;
        while ($targetQid !== '' && !isset($this->storage[$targetQid])) {
            $targetQid = substr($targetQid, 0, strlen($targetQid) - 1);
        }
        if ($targetQid === '') {
            throw new \Exception("unknown question id $qid or unknown code $code!");
        }
        return isset($this->storage[$targetQid][$code])
            ? $this->storage[$targetQid][$code]
            : $code;
    }

    /**
     * @param int $surveyId
     * @param string $language
     *
     * @return PredefinedAnswerCollection
     * @throws \CException
     */
    public static function load(int $surveyId, string $language): PredefinedAnswerCollection
    {
        return new self($surveyId, $language);
    }
}
