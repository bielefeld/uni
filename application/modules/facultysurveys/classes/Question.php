<?php


namespace application\modules\facultysurveys\classes;

use Exception;

/**
 * Class Question
 * @package application\modules\facultysurveys\classes
 * @property-read bool $isParent is true it is regarded as parent
 * @property-read int $questionId
 * @property-read int $gId
 * @property-read int $parentId the parent id. 0 is it's a parent otherwise greater than 0
 * @property-read string $type the question type
 * @property-read string $title the question title
 * @property-read string $content the question text
 * @property-read string $groupName the group name
 */
class Question
{
    /**
     * @var Question[]
     */
    private $children;
    /** @var string the question text */
    private $content;
    /** @var int the group id */
    private $gId;
    /** @var string the group name */
    private $groupName;
    /** @var int the parent id. 0 is it's a parent otherwise greater than 0 */
    private $parentId;
    /** @var int the question id */
    private $questionId;
    /**
     * @var int the id which marks the chosen sub question
     */
    private $responseId = -1;
    /** @var string the question title */
    private $title;
    /** @var string the question type */
    private $type;

    /** the Question constructor. Processes the given array
     * Question constructor.
     * @param array $data
     */
    private function __construct(array $data)
    {
        $this->questionId = (int)$data['qid'];
        $this->parentId = (int)$data['parent_qid'];
        $this->gId = (int)$data['gid'];
        $this->type = $data["type"];
        $this->title = $data["title"];
        $this->content = $data['question'];
        $this->isParent = $this->parentId === 0;
        $this->groupName = $data['group_name'];
    }

    public function __get($name)
    {
        return $this->{$name};
    }

    /** adds a child question to this object
     * @param Question $question the child questions
     * @throws \Exception
     */
    public function addChild(Question $question): void
    {
        if (!$this->isParent) {
            throw new Exception('only parents are allowed to have children!');
        }
        if ($question->isParent) {
            throw new Exception('A parent question can\'t be child of a parent question!');
        }
        $this->children[$question->title] = $question;
    }

    /** Returns the chosen child
     * @return Question
     */
    public function getActiveChild(): ?Question
    {
        if ($this->responseId === -1) {
            return null;
        }
        return $this->getChildren()[$this->responseId];
    }

    /**
     * @return Question[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @param string $subQuestionId
     * @return Question
     * @throws \Exception
     */
    public function getChild(string $subQuestionId): Question
    {
        if (!isset($this->children[$subQuestionId])) {
            throw new Exception("sub question $subQuestionId not found!");
        }
        return $this->children[$subQuestionId];
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getGId(): int
    {
        return $this->gId;
    }

    /**
     * @return int
     */
    public function getParentId(): int
    {
        return $this->parentId;
    }

    /**
     * @return int
     */
    public function getQuestionId(): int
    {
        return $this->questionId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /** Initializes a question with the given data
     * @param array $data the question data
     * @return Question the Question object
     */
    public static function init(array $data): Question
    {
        return new Question($data);
    }

    public function setResponseId(int $id): void
    {
        $this->responseId = $id;
    }
}
