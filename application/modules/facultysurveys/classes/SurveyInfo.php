<?php

namespace application\modules\facultysurveys\classes;

use CMap;
use Question;
use Response;
use Survey;
use SurveyLanguageSetting;
use Token;
use Yii;

/**
 * Class SurveyInfo
 *
 * <code>
 * $surveyInfo = new SurveyInfo(123456, 'de');
 * if ($surveyInfo->isValid()) {
 *     var_dump($surveyInfo->getName());
 * }
 * </code>
 *
 */
class SurveyInfo extends CMap
{

    /**
     * @var Survey
     */
    private $survey;

    /**
     * @var array
     */
    private $fieldmap;

    /**
     * @var array
     */
    private $columns;

    /**
     * @var string
     */
    private $language;

    /**
     * @var array Question
     */
    private $questions;

    /**
     * SurveyInfo constructor.
     *
     * @param Survey $survey
     * @param string $language
     * @throws \CException
     */
    public function __construct(Survey $survey, $language)
    {
        $result = SurveyLanguageSetting::model()
            ->findByPk(array(
                'surveyls_survey_id' => $survey->sid,
                'surveyls_language' => $language
            ));

        $data = $result != null ? array_merge($survey->getAttributes(), $result->getAttributes()) : array();
        parent::__construct($data, true);

        $this->survey = $survey;
        $this->language = $language;
    }

    /**
     * @return int
     */
    public function getSurveyId()
    {
        return $this->getSurvey()->sid;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return $this->getCount() > 0;
    }

    /**
     * @return Survey
     */
    public function getSurvey()
    {
        return $this->survey;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @return array
     */
    public function getFieldmap()
    {
        if (is_null($this->fieldmap)) {
            Yii::app()->loadHelper('common');
            $this->fieldmap = createFieldMap($this->survey, 'full', true, false, $this->language);
        }

        return $this->fieldmap;
    }

    /**
     * @param $c
     * @return bool
     */
    public function getSubquestionBySGQA($c)
    {
        $fieldmap = $this->getFieldmap();
        if (isset($fieldmap[$c]) && isset($fieldmap[$c]['subquestion'])) {
            return $fieldmap[$c]['subquestion'];
        } else {
            return false;
        }
    }

    /**
     * @param $title
     * @return mixed
     */
    public function getColumnNames($title)
    {
        if (isset($this->columns[$title])) {
            return $this->columns[$title];
        }

        $fieldmap = $this->getFieldmap();

        if (strpos($title, '_')) {
            list($questionCode, $subQuestionCode) = explode('_', $title);
        } else {
            $questionCode = $title;
            $subQuestionCode = false;
        }

        $fields = array();

        $response = Response::model($this->survey->sid);
        foreach ($response->getAttributes() as $key => $value) {
            if (array_key_exists($key, $fieldmap)) {
                if (array_key_exists('title', $fieldmap[$key]) && $fieldmap[$key]['title'] === $questionCode) {
                    if ($subQuestionCode === false) {
                        $fields[] = $key;
                    } else {
                        foreach ($fieldmap as $fKey => $def) {
                            if ($key != $fKey) continue;
                            if (isset($def['aid']) && $def['aid'] == $subQuestionCode) {
                                $fields[] = $fKey;
                            }
                        }
                    }
                }
            }
        }

        if (count($fields) > 0) {
            $this->columns[$title] = $fields;
            return $fields;
        }

        return false;
    }

    /**
     * @param $title
     * @return bool
     */
    public function getColumnName($title)
    {
        $columns = $this->getColumnNames($title);
        if (is_array($columns) && count($columns) > 0) {
            return $columns[0];
        }

        return false;
    }

    /**
     * @return mixed | null
     */
    public function getName()
    {
        return $this->itemAt('surveyls_title');
    }

    /**
     * @param $cssClass
     * @return array|static[]
     */
    public function getQuestionsByCssClass($cssClass)
    {
        $questions = Question::model()->with('questionAttributes')->findAllByAttributes(
            array(
                'sid' => $this->survey->sid,
                'language' => $this->language
            ),
            array(
                'order' => 'question_order'
            )
        );

        if (is_array($questions)) {
            $questions = array_filter($questions, function ($question) use ($cssClass) {

                foreach ($question->questionAttributes as $attribute) {
                    if ($attribute->attribute == 'cssclass' && strpos($attribute->value, $cssClass) !== false) {
                        return true;
                    }
                }

                return false;
            });
        }

        return $questions;
    }

    /**
     * @return float|int
     */
    public function getParticipationQuota()
    {
        $sid = $this->getSurveyId();

        $cntY = Token::model($sid)->countByAttributes(array(
            'completed' => 'Y'
        ));

        $cntN = Token::model($sid)->countByAttributes(array(
            'completed' => 'N'
        ));

        $cntAll = $cntY + $cntN;

        if ($cntAll > 0) {
            return round($cntY / $cntAll * 100);
        }

        else return 0;
    }

    public function titleize($token, $text)
    {
        return (strip_tags($text));
    }

    /**
     * @param $question
     * @param $subQuestion
     * @return string
     */
    public  function getSubQuestionColumnCode($question, $subQuestion)
    {
        return implode(
            'X',
            array(
                $question->getAttribute('sid'),
                $question->getAttribute('gid'),
                $question->getAttribute('qid')
            )). $subQuestion->title;
    }

    public function getResponses()
    {
        return Response::model($this->getSurveyId())->findAll(array(
            'condition' => 'submitdate IS NOT NULL'
        ));

    }

    /**
     * @param $title
     * @return null|Question
     */
    public function getQuestionByCode($questionCode)
    {
        $this->initQuestions();
        if (array_key_exists($questionCode, $this->questions)) {
            return $this->questions[$questionCode];
        }

        throw new QuestionNotFoundException('Question with title ' . $questionCode . ' not found');
    }

    private function initQuestions()
    {
        if (is_null($this->questions)) {
            $questions = Question::model()->findAllByAttributes(array(
                'sid' => $this->getSurveyId(),
                'language' => $this->getLanguage(),
            ));

            foreach ($questions as $question) {
                $title = $question->title;
                $this->questions[$title] = $question;
            }
        }
    }
}
