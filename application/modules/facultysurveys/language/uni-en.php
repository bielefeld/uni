<?php

return [
    'Antwort-ID' => 'response id',
    'Fakult&auml;lten und Einrichtungen' => 'faculties and institutions',
    'Einrichtung/Abteilung' => 'institute/Department',
    'Arbeitsgruppe' => 'work group',
    'Bauteil/Etage/R&auml;ume' => 'building/floor/room',
    'Verantwortliche(r)' => 'supervisor',
    'Betroffener' => 'name of affected person',
    'person:regular' => 'supervisor',
    'person:disabled' => 'name of affected person',
    'person:maternity' => 'expectant mother',
    'Datum' => 'date',
];
