<?php

return [
    'Antwort-ID' => 'Antwort-ID',
    'Fakult&auml;lten und Einrichtungen' => 'Fachbereiche, Verwaltung und zentrale Einrichtungen',
    'Einrichtung/Abteilung' => 'Abteilung',
    'Arbeitsgruppe' => 'Arbeitsgruppe',
    'Bauteil/Etage/R&auml;ume' => 'Bauteil/Etage/R&auml;ume',
    'person:regular' => 'Bearbeiter/in der Gefährdungsbeurteilung',
    'person:disabled' => 'Bearbeiter/in',
    'person:maternity' => 'Bearbeiter/in der Gefährdungsbeurteilung',
    'Datum' => 'Datum',
];
