<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>&Uuml;bersicht</title>
    <!-- Mainly scripts -->
    <script src="<?= $this->module->getAssetsUrl(); ?>/js/bootstrap.min.js"></script>
    <link href="<?= $this->module->getAssetsUrl(); ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?= $this->module->getAssetsUrl(); ?>/css/custom.css" rel="stylesheet">

    <link rel='icon' href='<?= $this->module->getAssetsUrl() ?>/img/favicon.ico' type='image/x-icon'/>
</head>

<body class="fixed-sidebar boxed-layout pace-done mini-navbar">

<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header" style="padding-left: 25px;">
                    <h2><?= $this->pageTitle; ?></h2>
                </div>
            </nav>
        </div>
        <div class="container">
            <a href="http://www.uni-bielefeld.de">
                <?= CHtml::image($this->module->getAssetsUrl() . '/img/logo.png', '',
                    [
                        'style' => 'height: 50px; margin-top: 10px; margin-bottom: 10px; margin-left: 25px;'
                    ]); ?>
            </a>
        </div>
        <div class="wrapper wrapper-content container">
            <div class="row">
                <div class="col-sm-9">
                    <?= $content; ?>
                </div>
            </div>
        </div>

    </div>
</div>

<footer>
    <div class="row greyfooter">
        <div class="copyright col-xs-12">
            © copyright Universität Bielefeld
            <br>
            <br>
            <a target="_blank" class="footerlink" href="https://www.uni-bielefeld.de/impressum/">Impressum</a> /
            <a target="_blank" class="footerlink" href="https://www.uni-bielefeld.de/datenschutzhinweise/">Datenschutz</a>
        </div>
    </div>
</footer>


</body>

</html>
