<?php

/** @var $this ModuleController */
/** @var Survey[] $surveys */

$class = "odd";
$this->pageTitle = 'Gef&auml;hrdungsbeurteilung - Fragebögen-Übersicht';

use application\modules\facultysurveys\classes\SurveyInfo; ?>
<h2>Übersicht</h2>
<div id="index">
    <div class="container">
        <?php foreach ($surveys as $survey) {
            $class = ($class === 'odd') ? 'even' : 'odd';
            ?>
            <div class="row current even">
                <a href="<?= $survey->getsSurveyUrl() ?>">
                    <?= (new SurveyInfo($survey, 'de'))->getName() ?>
                </a>
            </div>
            <?php
        }
        ?>
        <br>
        <div>
            <p><a href="<?= $this->createUrl('index') ?>">zur&uuml;ck zur Startseite</a></p>
        </div>
    </div>
</div>
