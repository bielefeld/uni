<?php
/** @var $this ModuleController */
/** @var string $url */
/** @var bool $isPrintable */
/** @var string $type */
/** @var string $language */
/** @var string $data the html content of the answers */

$this->pageTitle = '';

if ($isPrintable) {
    $this->layout = 'print';
}

?>

<?= $data ?>

<?php
if ($type !== null) {
    include "extras/{$type}_{$language}.php";
} else {
    include "extras/regular_{$language}.php";
}
if ($isPrintable) {
    echo '<script>window.print()</script>';
}
?>
