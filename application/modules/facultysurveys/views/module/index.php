<?php
/** @var $this ModuleController */
?>

<?php
$this->pageTitle = 'Gef&auml;hrdungsbeurteilung - &Uuml;bersicht';

?>

<h2>Übersicht</h2>
<div id="index">
    <div class="container">
        <div class="row current odd"><a href="../upload/files/Tutorial.pdf" id="surveylist-container">Tutorial</a>
        </div>
        <div class="row current even"><a href=<?= $this->createUrl('registration?surveyId=85328') ?>>Registrierung</a>
        </div>
        <div class="row current odd"><a href="<?= $this->createUrl('surveyoverview') ?>">Fragebögen-Übersicht</a></div>
<!--        <div class="row current even"><a href="#">FAQ</a></div>-->
        <div class="row current odd"><a href="mailto:gbu@uni-bielefeld.de">Kontakt</a></div>
    </div>
</div>

<h2>Gefährdungsbeurteilung allgemein</h2>
<p>
    Die Gefährdungsbeurteilung ist das zentrale Element im Arbeits- und
    Gesundheitsschutz und bildet die Grundlage für die sichere und gesunde
    Gestaltung von Arbeitsabläufen und –plätzen. Auf Basis der Ergebnisse der
    Gefährdungsbeurteilung werden Unfälle bei der Arbeit verhindert und der Arbeits-
    und Gesundheitsschutz optimiert, indem mögliche Gefährdungen (einschließlich
    ihrer Wechselwirkungen) bewertet und geeignete Verbesserungsmaßnahmen formuliert
    und umgesetzt werden.
</p>
<p>
    Zuständig für die regelmäßige Durchführung von Gefährdungsbeurteilungen ist die
    jeweilige Führungskraft eines Verantwortungsbereichs. Die Aufgabe kann aber auch
    an eine andere, sachkundige Person übertragen werden, in diesem Fall übernimmt
    die Führungskraft eine Kontrollfunktion.
</p>
<p>Rechtliche Regeln zur Gefährdungsbeurteilung:</p>
<ul>
    <li>Arbeitsschutzgesetz</li>
    <li>BGV A1 Grundsätze der Prävention</li>
    <li>Gefahrstoffverordnung</li>
    <li>Biostoffverordnung</li>
    <li>Betriebssicherheitsverordnung</li>
    <li>Verweise in zahlreichen Arbeitsschutznormen</li>
</ul>
<h2>Aufbau und Konzept der Online-Gefährdungsbeurteilung</h2>
<p>
    Die Durchführdung einer Online-Gefährdungsbeurteilung soll der verantwortlichen
    Person eines Bereichs auf unkomplizierte Weise dabei helfen, zu überprüfen, ob
    bestimmte Anforderungen zum Arbeitsschutz/zur Arbeitssicherheit in ihrem Bereich
    erfüllt sind.
</p>
<p>
    Die Überprüfung erfolgt dabei anhand von digitalen Fragebögen, die mit der
    Online-Umfrage-Applikation „Limesurvey“ umgesetzt wurden.
</p>
<p>
    Ein Fragebogen besteht aus übergeordneten Hauptfragen und aus Detailfragen, die die
    Erfüllung der Anforderungen genauestens überprüfen. Sind die Anforderungen für
    einen Themenkomplex mit Sicherheit erfüllt, so beantwortet der Verantwortliche
    die jeweilige übergeordnete Frage mit „Ja“ und gelangt anschließend zum nächsten
    Punkt. Ist er sich jedoch bei einem Themenbereich nicht sicher, hat er die
    Möglichkeit diese Frage mit „Nicht sicher“ zu beantworten. Daraufhin erfolgt
    eine detaillierte Prüfung auf Erfüllung der Anforderungen anhand von
    Einzelfragen. Diese bestehen aus einer „Ja/Nein“-Abfrage. Sind die Sachverhalte
    ganz oder teilweise in einem Verantwortungsbereich nicht relevant, hat der
    Befragte die Möglichkeit mit „nicht zutreffend“ zu antworten. Anschließend wird
    automatisch erkannt, ob die Angaben den erforderlichen Anforderungen
    entsprechen. Ist dies nicht der Fall, hat der Befragte die Möglichkeit in einem
    „Freitext“-Feld Maßnahmen anzugeben, die zukünftig umgesetzt werden sollen, um
    mögliche Gefährdungen auszuschließen. Zudem besteht die Möglichkeit, zu
    markieren, dass externe Hilfe durch Fachkräfte benötigt wird, um eine bestehende
    Gefährdung zu beseitigen.
</p>
<p>
    Am Ende jedes Fragebogens wird das persönliche Ergebnis angezeigt. Durch eine
    farbliche Unterscheidung lässt sich hier auf einen Blick erkennen, in welchen
    zuvor abgefragten Bereichen Verbesserungspotential besteht (rot) und für welche
    Bereiche die Anforderungen bereits vollständig erfüllt sind.
</p>
<p>
    Neben dem jeweiligen Benutzer hat die für Arbeitsschutz und Arbeitssicherheit
    zuständige Abteilung die Möglichkeit zur Einsichtnahme in die Ergebnisse der
    durchgeführten Gefährdungsbeurteilungen. Sie kann den Verantwortlichen so eine
    Unterstützung bei der Schwachstellenbeseitigung bieten und zusätzlich anhand
    aller verfügbaren Gefährdungsbeurteilungen weitere Analysen durchführen und
    hochschulweite Problemfelder feststellen und beseitigen.
</p>
