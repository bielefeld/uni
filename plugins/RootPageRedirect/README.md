# LimeSurvey RootPageRedirect Plugin

The Limesurvey RootPageRedirect plugin allows you to redirect to any URL when a user tries to enter the survey listing page.


## Plugin Installation

- Copy the RootPageRedirect folder to the Limesurvey "plugins" directory.
- Activate the plugin at the Limesurvey plugin manager (requires proper user rights for accessing the feature at the Limesurvey admin interface).
- Configure the Redirect URL at the settings page

![New Icon in response overview](./settings.png)
