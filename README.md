#Bielefeld-Modul
## Über
das Bielefeld-Modul stellt für abgeschlossene Umfragen E-Mailbenachrichtigungen sowie (Druck-)Ansichten bereit.
### Installation
* Das Verzeichnis in das *application/modules* reinkopieren
* in *application/config.php* folgendes unter 'modules' einfügen:
<code>  
'modules' => [  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'facultysurveys' => [  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'class' => FacultySurveysModule::class,  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**'responsible'** => 'code für Verantwortlicher',  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**'department'** => 'code für Einrichtung',  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**'furtherData'** => 'code für Weitere Angaben',  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**'faculty'** => 'code für Fakultät',  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**'facultyMode'** => 'Modus' ('fh' oder 'uni'),   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;],  
],  
</code>
* unter 'components' bei dem URL-Manager 'showScriptName' ggf. auf **false** setzen
##offene Punkte

1. Anpassen der Link in Admin benachrichtigung bei registrierungsumfrage  
a) generischen link setzen für Antwort bearbeiten  
b) link auf freischaltung anpassen, es fehlt wohl "dev. ...", evtl. über platzhalter lösbar? -> schaue ich mir an
